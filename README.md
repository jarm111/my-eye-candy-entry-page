# My Eye Candy Entry Page

An entry page demonstrating the application of CSS3 and JavaScript.

## Acknowledgments

* Wikimedia Commons: Pictures
* Meyerweb: CSS Reset
* Justin Windle: Text Scramble Effect
* WebSonick: Night sky with twinkling stars
* nbartlomiej: Foggy